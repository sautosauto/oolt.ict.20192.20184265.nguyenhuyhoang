package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.order.Order;

public class DiskTest {
    public static void main(String[] args) {
        var anOrder = new Order();

        var dvd1 = new DigitalVideoDisc("The Lion King");
        dvd1.setCategory("Animation");
        dvd1.setCost(19.95f);
        dvd1.setDirector("Roger Allers");
        dvd1.setLength(87);
        anOrder.addMedia( dvd1);

        var dvd2 = new DigitalVideoDisc("Star Lion");
        dvd2.setCategory("Science Fiction");
        dvd2.setCost(24.95f);
        dvd2.setDirector("George Lucas");
        dvd2.setLength(124);
        anOrder.addMedia( dvd2);

        var dvd3 = new DigitalVideoDisc("Aladdin Star King");
        dvd3.setCategory("Animation");
        dvd3.setCost(18.99f);
        dvd3.setDirector("John Musker");
        dvd3.setLength(90);

        anOrder.addMedia( dvd3);


        //test search method

        System.out.println(new StringBuilder().append("Search for \"King Lion\" in dvd1: ")
                            .append( dvd1.search("King Lion")).toString());

        System.out.println(new StringBuilder().append("Search for \"King Lion\" in dvd2: ")
                            .append( dvd2.search("King Lion")).toString());

        System.out.println(new StringBuilder().append("Search for \"King Lion\" in dvd3: ")
                            .append( dvd3.search("King Lion")).toString());


        // test getLuckyItem()
        anOrder.print();

        anOrder.getALuckyItem();
        anOrder.print();


        System.exit(0);
    }
}
