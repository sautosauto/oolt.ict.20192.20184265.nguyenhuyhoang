package hust.soict.globalict.aims;

import hust.soict.globalict.aims.media.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Aims {

    public static void showMenu() {
        System.out.println("\nOrder Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4\n");
    }

    public static void main(String[] args) {

//        var myDaemon = new MemoryDaemon();
//        myDaemon.start();
//
//        var x = new OrderExecutor();
//        do {
//            showMenu();
//        }
//        while ( x.execute() == true);
//
//
//        System.out.println("Exiting ....");
//        System.out.println("Memory used: " + myDaemon.getMemoryUsed());


        System.out.println("\nAnother test:");
        List<Media> collection = new ArrayList<>();

        collection.add(
                new DigitalVideoDisc(
                        "Hello there", "Family", "Alexander the II",
                        16, 200)
        );


        var myTracks = new ArrayList<Track>();
        myTracks.add(new Track("Soda Dance", 40));
        myTracks.add(new Track("Nano Bird", 10));

        collection.add(
                new CompactDisc(new Disc("Friendly Music",
                        "Funk",
                        "Hotona",
                        17,
                        300F), myTracks));


        myTracks.remove(0);
        collection.add(
                new CompactDisc(new Disc("Unfriendly Music",
                        "Classic",
                        "Hotonama",
                        20,
                        300F), myTracks));


        String[] m = {"Polar Beer", "Julong", "ChIcKeNhEaD"};
        List<String> myAuthors = new ArrayList<>(Arrays.asList(m));
        collection.add(
                new Book("A Undying rok",
                        "History",
                        36F,
                        myAuthors)
        );


        System.out.println("Current order:");
        for (var p : collection) {
            p.print();
        }

        System.out.println("Sorted order:");

        Collections.sort((List) collection);
        System.out.println("Current order:");
        for (var p : collection) {
            p.print();
        }

        System.exit(0);
    }
}
