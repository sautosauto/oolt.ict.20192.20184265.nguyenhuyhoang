package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable<Media>{

    //Constructors
    public DigitalVideoDisc(){}

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

    public DigitalVideoDisc(String title,
                            String category,
                            String director) {
        super(title, category);
        this.director = director;
    }

    public DigitalVideoDisc(String title,
                            String category,
                            String director,
                            int length,
                            float cost) {
        super(title,category, cost);
        this.director = director;
        this.length = length;
    }

    public DigitalVideoDisc( Disc d){
        super( d.title, d.category, d.cost);
        this.director =d.director;
        this.length = d.length;
    }


    public boolean search(String name){
        if (name.isEmpty()) return false;

        for (String s: name.split(" ")) {
            if( ! title.contains(s))
                return false;
        }
        return true;
    }


    @Override
    public void play() throws PlayerException {
        if (this.getLength() <=0){
            System.err.println("ERROR: DVD length is 0");
            throw new PlayerException();
        }
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }

    @Override
    public DigitalVideoDisc interactiveInitialize() throws PlayerException {
        Disc d = super.interactiveInitialize();
        return new DigitalVideoDisc(d);
    }


    @Override
    public int compareTo(Media media) {
        if (this.getCost() > media.getCost()){
            return 1;
        }
        else {
            return 0;
        }
    }
}

