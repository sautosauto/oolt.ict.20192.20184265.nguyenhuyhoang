package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public interface interactiveInitializable {
    Object interactiveInitialize() throws PlayerException;
}
