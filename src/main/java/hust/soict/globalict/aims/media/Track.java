package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

import java.util.Scanner;

public class Track implements Playable, Comparable<Media>{
    private String title;
    private int length;

    //Constructors
    public Track(){}

    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }


    //getters
    public String getTitle() {
        return title;
    }

    public int getLength() {
        return length;
    }

    //setters
    public void setTitle(String title) {
        this.title = title;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public void play() throws PlayerException {
        if (this.getLength() <=0){
            System.err.println("ERROR: Track length is 0");
            throw new PlayerException();
        }
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }


    public Track interactiveInitialize(){
        Scanner s = new Scanner( System.in);
        System.out.println("Input track title: ");
        String title = "";
        try {
            title = s.nextLine().trim();
        } catch (Exception e){
            e.printStackTrace();
        }

        System.out.println("Input track length: ");
        int length = 0;
        try {
            length = s.nextInt();
        } catch (Exception e){
            e.printStackTrace();
        }

        return new Track( title, length);
    }


    @Override
    public int compareTo(Media media) {
        return this.getTitle().compareTo( media.getTitle());
    }
}
