package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

import java.util.Scanner;

public class Disc extends Media{
    protected int length;
    protected String director;

    //Constructors
    public Disc(){}

    public Disc(String title) {
        super(title);
    }

    public Disc(String title, String category) {
        super(title, category);
    }

    public Disc(String title, String category, float cost) {
        super(title, category, cost);
    }

    public Disc(String title, String category, String director, int length, float cost) {
        super(title, category, cost);
        this.director = director;
        this.length = length;
    }

    public Disc( Disc d){
        super( d.title, d.category, d.cost);
        this.director = d.director;
        this.length = d.length;
    }

    @Override
    public Disc interactiveInitialize() throws PlayerException {
        Scanner x = new Scanner( System.in);
        System.out.println("Interactive disc Creation");

        System.out.println("Input disc title: ");
        String title = x.nextLine().trim();

        System.out.println("Input category: ");
        String category = x.nextLine().trim();

        System.out.println("Input disc director: ");
        String director = x.nextLine().trim();

        System.out.println("Input disc length: ");


        int length = 0;
        try {
            length = Integer.parseInt( x.nextLine());
        }
        catch (NumberFormatException e){
            System.out.println("Illegal format!");
            e.printStackTrace();
            System.exit(1);
        }


        System.out.println("Input disc cost: ");
        float cost = 0L;
        try {
            cost = Float.parseFloat( x.nextLine());
        }
        catch (NumberFormatException e){
            System.out.println("Illegal format!");
            e.printStackTrace();
            System.exit(1);
        }

        return new Disc(title, category, director, length, cost);
    }


    @Override
    public void print() {
        //1. DVD - [Id] - [Title] - [category] - [Director] - [Length]: [Price] $
        System.out.println(
                "DVD - " +
                        getId() + " - " +
                        getTitle() + " - " +
                        getCategory() + " - " +
                        getDirector() + " - " +
                        getLength() + ":  " +
                        getCost() + "$");
    }


    //getters
    public int getLength() {
        return length;
    }

    public String getDirector() {
        return director;
    }

    //setters
    public void setLength(int length) {
        this.length = length;
    }

    public void setDirector(String director) {
        this.director = director;
    }
}
