package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

import java.util.ArrayList;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable, Comparable<Media>{
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<>();

    //Constructors
    public CompactDisc(){}

    public CompactDisc(Disc disc){
        this.title = disc.title;
        this.director = disc.director;
        this.id = disc.id;
        this.category = disc.category;
    }

    public CompactDisc(String title, String category, String artist){
        super(title, category);
        this.artist = artist;
        this.length = getLength();
    }

    public CompactDisc( Disc d, ArrayList<Track> tracks){
        super(d);
        this.tracks = tracks;
        this.length = getLength();
    }


    public void addTrack( Track newTrack){
        if ( tracks.contains( newTrack)){
            System.out.println("Track already existed!");
        }
        else {
            tracks.add( newTrack);
            this.length = getLength();
        }
    }

    public void removeTrack(Track deleteTrack){
        if ( !tracks.contains( deleteTrack)){
            System.out.println("Track does not exist!");
        }
        else {
            tracks.remove( deleteTrack);
            this.length = getLength();
        }
    }


    //getters
    public String getArtist() {
        return artist;
    }

    @Override
    public int getLength() {
        int length = 0;
        if (tracks != null) {
            for ( var track: tracks){
                length += track.getLength();
            }
        }
        return length;
    }

    //setters
    public void setArtist(String artist) {
        this.artist = artist;
    }


    @Override
    public void play() {
        if (this.getLength() <=0){
            System.err.println("ERROR: CD length is 0");
            try {
                throw new PlayerException();
            } catch (PlayerException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
                System.out.println(e.toString());
            }
        }

        for (Track t: tracks){
            System.out.println("Now playing: " + t.getTitle());
            try {
             t.play();
            } catch (PlayerException e){
                e.printStackTrace();
            }

            try {
                Thread.sleep( Math.max(t.getLength()*50, 500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public CompactDisc interactiveInitialize() throws PlayerException {
        Disc d = super.interactiveInitialize();

        Scanner s = new Scanner( System.in);
        System.out.println("Input number of tracks: ");
        int numTrack = 0;
        try{
            numTrack = s.nextInt();
        } catch (Exception e){
            e.printStackTrace();
        }

        ArrayList<Track> trackArray = new ArrayList<>();
        for (int i = 0; i < numTrack; i++) {
            System.out.println(
                    "Input track " + i + " infomation: ");

            Track t = new Track();
            t.interactiveInitialize();
            trackArray.add( t.interactiveInitialize());
        }
        var newDisk = new CompactDisc(d, trackArray);
        newDisk.interactivePlay();
        return newDisk;
    }

    @Override
    public int compareTo(Media media) {
        if ( media instanceof CompactDisc) {
            if (this.tracks.size() > ((CompactDisc) media).tracks.size()) {
                return 1;
            } else if (this.tracks.size() < ((CompactDisc) media).tracks.size()) {
                return -1;
            } else if (this.length > ((CompactDisc) media).length) {
                return 1;
            } else {
                return -1;
            }
        }
        else {
            return this.getTitle().compareTo( media.getTitle());
        }
    }
}
