package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

import java.util.UUID;

public abstract class Media implements interactiveInitializable, Comparable<Media> {
    protected String id;
    protected String title;
    protected String category;
    protected float cost;

    public Media() {
    }
    //Constructors

    public Media(String title) {
        this.title = title;
        this.id = UUID.randomUUID().toString();
    }

    public Media(String title, String category) {
        this.title = title;
        this.category = category;
        this.id = UUID.randomUUID().toString();
    }

    public Media(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
        this.id = UUID.randomUUID().toString();
    }


    public abstract Media interactiveInitialize() throws PlayerException;

    public abstract void print();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        if (!(o instanceof Media)) return false;
        Media media = (Media) o;

        if (Float.compare(media.getCost(), getCost()) != 0) return false;
        return getTitle() != null ? getTitle().equals(media.getTitle()) : media.getTitle() == null;
    }


    @Override
    public int compareTo(Media media) {

        if (this.equals(media)) return 0;

        else {
            if (Float.compare(media.getCost(), getCost()) > 0) return 1;
            else if (getTitle().compareTo(media.getTitle()) > 0) return 1;
            else return -1;
        }
    }

    //    Setters
    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }


    //    Getters
    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public float getCost() {
        return cost;
    }

    public String getId() {
        return id;
    }
}
