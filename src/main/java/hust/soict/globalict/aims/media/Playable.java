package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

import java.util.Scanner;

public interface Playable {
    void play() throws PlayerException;

    default void interactivePlay() throws PlayerException {
        Scanner s = new Scanner( System.in);
        System.out.println("Press 1 to play media, others for cancel");
        int option = 0;
        try{
            option = s.nextInt();
        } catch (Exception e){
            e.printStackTrace();
        }

        if (option == 1){
            play();
        }
    }
}
