package hust.soict.globalict.aims.media;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

public class Book extends Media implements Comparable<Media> {

    private List<String> authors = new ArrayList<String>();
    private String content;
    private List<String> contentTokens;
    private Map<String, Integer> wordFrequency;


    //Constructors
    public Book() {
    }

    public Book(String title) {
        super(title);
    }

    public Book(String title,
                String category) {
        super(title, category);
        this.authors = authors;
    }

    public Book(String title,
                String category,
                float cost,
                List<String> authors) {
        super(title, category, cost);
        this.authors = authors;
    }

    @Override
    public Book interactiveInitialize() {
        Scanner x = new Scanner(System.in);
        System.out.println("Interactive Book Creation");

        System.out.println("Input book title: ");
        String title = x.nextLine().trim();

        System.out.println("Input book category: ");
        String category = x.nextLine().trim();

        System.out.println("Input book directors:( authorA, authorB, ... ) ");
        List<String> authors;
        authors = Arrays.asList(x.nextLine().trim().split(", "));

        System.out.println("Input book cost: ");
        float cost = 0L;
        try {
            cost = x.nextFloat();
        } catch (NumberFormatException e) {
            System.out.println("Illegal format!");
            e.printStackTrace();
            System.exit(1);
        }

        return new Book(title, category, cost, authors);
    }

    void addAuthor(String authorName) {
        if (!authors.contains(authorName)) {
            authors.add(authorName);
        }
    }

    void removeAuthor(String authorName) {
        authors.remove(authorName);
    }


    //Setters
    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }


    //Getters
    public List<String> getAuthors() {
        return authors;
    }

    @Override
    public void print() {
        //1. Book - [Id] - [Title] - [category] - [Authors]: [Price] $
        System.out.println(
                new StringBuilder().append("Book - ")
                        .append(getId()).append(" - ")
                        .append(getTitle()).append(" - ")
                        .append(getCategory()).append(" - ")
                        .append(getAuthors().toString()).append(": ")
                        .append(getCost()).append("$"));
    }


    @Override
    public int compareTo(Media media) {
        return this.getTitle().compareTo(media.getTitle());
    }

    public void setContent(String content) {
        this.content = content;

//        Unique tokens
        this.contentTokens = Arrays.asList(content.split("[ ,.\\[\\]?!(){}]+"));
        this.contentTokens = contentTokens.stream().distinct().collect(Collectors.toList());
//        Order a->z
        Collections.sort(contentTokens,
                new Comparator<String>() {
                    @Override
                    public int compare(String s, String t1) {
                        return s.toUpperCase().compareTo(t1.toUpperCase());
                    }
                });

        this.wordFrequency = new HashMap<>();

        for (var x : contentTokens) {
            int occurrence = StringUtils.countMatches(content, x);
            this.wordFrequency.put(x, occurrence);
        }
    }

    @Override
    public String toString() {
        return "Title: " + title + "\n" +
                "Category: " + category + "\n" +
                "Authors: " + authors.toString() + "\n" +
                "Length: " + content.length() + "\n" +
                "Token list: " + contentTokens.toString() + "\n" +
                "Frequency: " + wordFrequency.toString() + "\n" +
                "Cost: " + cost + "\n";
    }
}
