package hust.soict.globalict.aims.utils.DateParsers;

// Divide into 2 upper, 2 lower for year
// US format

import org.apache.commons.lang3.StringUtils;

import static hust.soict.globalict.aims.utils.DateParsers.formatDate.printFormatter;

public final class YearParser {

    public static enum upperDigits {
        ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5),
        SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10),
        ELEVEN(11), TWELVE(12), THIRTEEN(13), FOURTEEN(14), FIFTEEN(15),
        SIXTEEN(16), SEVENTEEN(17), EIGHTEEN(18), NINETEEN(19), TWENTY(20),
        TWENTY_ONE(21), TWENTY_TWO(22), TWENTY_THREE(23), TWENTY_FOUR(24), TWENTY_FIVE(25),
        TWENTY_SIX(26), TWENTY_SEVEN(27), TWENTY_EIGHT(28), TWENTY_NINE(29), THIRTY(30),
        THIRTY_ONE(31), THIRTY_TWO(32), THIRTY_THREE(33), THIRTY_FOUR(34), THIRTY_FIVE(35),
        THIRTY_SIX(36), THIRTY_SEVEN(37), THIRTY_EIGHT(38), THIRTY_NINE(39), FORTY(40),
        FORTY_ONE(41), FORTY_TWO(42), FORTY_THREE(43), FORTY_FOUR(44), FORTY_FIVE(45),
        FORTY_SIX(46), FORTY_SEVEN(47), FORTY_EIGHT(48), FORTY_NINE(49), FIFTY(50),
        FIFTY_ONE(51), FIFTY_TWO(52), FIFTY_THREE(53), FIFTY_FOUR(54), FIFTY_FIVE(55),
        FIFTY_SIX(56), FIFTY_SEVEN(57), FIFTY_EIGHT(58), FIFTY_NINE(59), SIXTY(60),
        SIXTY_ONE(61), SIXTY_TWO(62), SIXTY_THREE(63), SIXTY_FOUR(64), SIXTY_FIVE(65),
        SIXTY_SIX(66), SIXTY_SEVEN(67), SIXTY_EIGHT(68), SIXTY_NINE(69), SEVENTY(70),
        SEVENTY_ONE(71), SEVENTY_TWO(72), SEVENTY_THREE(73), SEVENTY_FOUR(74), SEVENTY_FIVE(75),
        SEVENTY_SIX(76), SEVENTY_SEVEN(77), SEVENTY_EIGHT(78), SEVENTY_NINE(79), EIGHTY(80),
        EIGHTY_ONE(81), EIGHTY_TWO(82), EIGHTY_THREE(83), EIGHTY_FOUR(84), EIGHTY_FIVE(85),
        EIGHTY_SIX(86), EIGHTY_SEVEN(87), EIGHTY_EIGHT(88), EIGHTY_NINE(89), NINETY(90),
        NINETY_ONE(91), NINETY_TWO(92), NINETY_THREE(93), NINETY_FOUR(94), NINETY_FIVE(95),
        NINETY_SIX(96), NINETY_SEVEN(97), NINETY_EIGHT(98), NINETY_NINE(99);

        private int value;

        upperDigits(int value) {
            this.value = value;
        }

        static int getValue(String a) {
            try {
                return upperDigits.valueOf(a).value;
            } catch (Exception e) {
                System.out.println("Something's wrong");
                System.exit(0);
            }
            return 0;
        }
    }

    public static enum lowerDigits {
        HUNDRED(0), O_ONE(1), O_TWO(2), O_THREE(3), O_FOUR(4),
        O_FIVE(5), O_SIX(6), O_SEVEN(7), O_EIGHT(8), O_NINE(9),
        TEN(10), ELEVEN(11), TWELVE(12), THIRTEEN(13), FOURTEEN(14),
        FIFTEEN(15), SIXTEEN(16), SEVENTEEN(17), EIGHTEEN(18), NINETEEN(19),
        TWENTY(20), TWENTY_ONE(21), TWENTY_TWO(22), TWENTY_THREE(23), TWENTY_FOUR(24),
        TWENTY_FIVE(25), TWENTY_SIX(26), TWENTY_SEVEN(27), TWENTY_EIGHT(28), TWENTY_NINE(29),
        THIRTY(30), THIRTY_ONE(31), THIRTY_TWO(32), THIRTY_THREE(33), THIRTY_FOUR(34),
        THIRTY_FIVE(35), THIRTY_SIX(36), THIRTY_SEVEN(37), THIRTY_EIGHT(38), THIRTY_NINE(39),
        FORTY(40), FORTY_ONE(41), FORTY_TWO(42), FORTY_THREE(43), FORTY_FOUR(44),
        FORTY_FIVE(45), FORTY_SIX(46), FORTY_SEVEN(47), FORTY_EIGHT(48), FORTY_NINE(49),
        FIFTY(50), FIFTY_ONE(51), FIFTY_TWO(52), FIFTY_THREE(53), FIFTY_FOUR(54),
        FIFTY_FIVE(55), FIFTY_SIX(56), FIFTY_SEVEN(57), FIFTY_EIGHT(58), FIFTY_NINE(59),
        SIXTY(60), SIXTY_ONE(61), SIXTY_TWO(62), SIXTY_THREE(63), SIXTY_FOUR(64),
        SIXTY_FIVE(65), SIXTY_SIX(66), SIXTY_SEVEN(67), SIXTY_EIGHT(68), SIXTY_NINE(69),
        SEVENTY(70), SEVENTY_ONE(71), SEVENTY_TWO(72), SEVENTY_THREE(73), SEVENTY_FOUR(74),
        SEVENTY_FIVE(75), SEVENTY_SIX(76), SEVENTY_SEVEN(77), SEVENTY_EIGHT(78), SEVENTY_NINE(79),
        EIGHTY(80), EIGHTY_ONE(81), EIGHTY_TWO(82), EIGHTY_THREE(83), EIGHTY_FOUR(84),
        EIGHTY_FIVE(85), EIGHTY_SIX(86), EIGHTY_SEVEN(87), EIGHTY_EIGHT(88), EIGHTY_NINE(89),
        NINETY(90), NINETY_ONE(91), NINETY_TWO(92), NINETY_THREE(93), NINETY_FOUR(94),
        NINETY_FIVE(95), NINETY_SIX(96), NINETY_SEVEN(97), NINETY_EIGHT(98), NINETY_NINE(99);

        private int value;

        lowerDigits(int value) {
            this.value = value;
        }

        static int getValue(String a) {
            try {
                return lowerDigits.valueOf(a).value;
            } catch (Exception e) {
                System.out.println("Something's wrong");
                System.exit(0);
            }
            return 0;
        }
    }


    public static int match(String year) {
        year = year.trim().toUpperCase().replace("-", "_");
        if( year.contains(" ")) {
            String upper = year.split(" ")[0];
            String lower = StringUtils.difference( upper, year).trim().replace(" ", "_");
            return getYearDigit(upper, lower);
        }
        else {
            return getYearDigit(year);
        }
    }


    private static int getYearDigit(String upper, String lower) {
        int year = upperDigits.getValue(upper) * 100 + lowerDigits.getValue(lower);
        return year;
    }

    private static int getYearDigit(String upper) {
        int year = upperDigits.getValue(upper);
        return year;
    }


    public static String intToString(int yearNum) {
        try {
            int upperDigit = yearNum / 100;
            int lowerDigit = yearNum % 100;
            var yearString = printFormatter(YearParser.upperDigits.values()[upperDigit - 1].toString())
                    + " "
                    + printFormatter(YearParser.lowerDigits.values()[lowerDigit].toString());

            return yearString;
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            System.exit(0);
        }
        return null;
    }

    public static void main(String[] args) {
        System.out.println(YearParser.match("twenty-five sixty-two"));
    }
}
