package hust.soict.globalict.aims.utils;

import org.joda.time.DateTime;

import java.util.Arrays;
import java.util.List;

public class DateUtils {
    public static int compareDate(DateTime date1, DateTime date2) {
        // date1 == date2 -> return 0
        // date1 < date2 -> return -1
        // date1 == date2 -> return 1
        try {
            return date1.compareTo(date2);
        } catch (Exception e) {
            System.out.println("Something went wrong");
            System.exit(0);
        }
        return Integer.parseInt(null);
    }

    public static void sortDate(DateTime[] arr) {
        List<DateTime> list = Arrays.asList(arr);
        list.sort(DateTime::compareTo);
    }

    public static void main(String[] args) {

        var arrDate = new DateTime[5];
        arrDate[0] = new DateTime(2019, 7, 22, 0, 0, 0, 0);
        arrDate[1] = new DateTime(2016, 7, 22, 0, 0, 0, 0);
        arrDate[2] = new DateTime(2021, 7, 22, 0, 0, 0, 0);
        arrDate[3] = new DateTime(2013, 7, 22, 0, 0, 0, 0);
        arrDate[4] = new DateTime(2017, 7, 22, 0, 0, 0, 0);

        sortDate(arrDate);
        for (int i = 0; i < arrDate.length; i++) {
            System.out.println(arrDate[i].toString());
        }

        System.exit(0);
    }

}
