package hust.soict.globalict.aims.utils;

import hust.soict.globalict.aims.utils.DateParsers.DayParser;
import hust.soict.globalict.aims.utils.DateParsers.MonthParser;
import hust.soict.globalict.aims.utils.DateParsers.YearParser;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Scanner;

public class MyDate {

    private int day ;
    private int month;
    private int year;

    public MyDate(){
        //Current time
        var jodaTime = new DateTime();
        this.day = jodaTime.getDayOfMonth();
        this.month = jodaTime.getMonthOfYear();
        this.year = jodaTime.getYear();
    }

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public void accept(){
        var scanner = new Scanner( System.in);
        System.out.println("Input date in format dd-mm-yyyy: ");

        String[] dateList  =  scanner.nextLine().trim().split("-");
        setDay( Integer.parseInt( dateList[0]));
        setMonth( Integer.parseInt( dateList[1]));
        setYear(Integer.parseInt(dateList[2]));
    }


    public void printDigitFormat() {
        System.out.println("Date: " +
                day + "-" + month + "-" + year);
    }


    public void print() {
        String datetimeString = MonthParser.intToString(month) + " the " + DayParser.intToString(day)
                + ", " + YearParser.intToString(year);
        System.out.println(datetimeString);
    }

    public void print(String pattern) {

        DateTime dt = getDateTime();
//        String pattern = "dd-MM-YYYY";
        DateTimeFormatter df = DateTimeFormat.forPattern(pattern);

        System.out.println(df.print(dt));
    }


    //Setters
    public void setDay(int day) {
        this.day = day;
    }

    public void setDay(String day) {
        this.day = DayParser.match(day);
    }


    public void setMonth(int month) {
        this.month = month;
    }

    public void setMonth(String month) {
        this.month = MonthParser.match(month);
    }


    public void setYear(int year) {
        this.year = year;
    }

    public void setYear(String year) { this.year = YearParser.match( year); }


    //Getters
    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public DateTime getDateTime() {
        return DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(day + "/" + month + "/" + year);
    }
}
