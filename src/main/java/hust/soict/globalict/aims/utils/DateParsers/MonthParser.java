package hust.soict.globalict.aims.utils.DateParsers;

import static hust.soict.globalict.aims.utils.DateParsers.formatDate.printFormatter;

public enum MonthParser {

    JANUARY(1), FEBRUARY(2), MARCH(3), APRIL(4), MAY(5),
    JUNE(6), JULY(7), AUGUST(8), SEPTEMBER(9),
    OCTOBER(10), NOVEMBER(11), DECEMBER(12);

    private int value;

    MonthParser(int value) {
        this.value = value;
    }

    public static int match(String month) {
        month = month.trim().toUpperCase();
        int monthInt = 0;
        try {
            monthInt = MonthParser.valueOf(month).value;
        } catch (Exception e) {
            System.out.println("Invalid input");
            System.exit(0);
        }
        return monthInt;
    }

    public static String intToString(int monthNum) {
        try {
            return printFormatter(MonthParser.values()[monthNum - 1].toString());
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            System.exit(0);
        }
        return null;
    }


    public static void main(String[] args) {

        System.out.println(MonthParser.match("january"));
    }
}
