package hust.soict.globalict.aims.memoryTracker;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class MemoryDaemon implements Runnable{
    private long memoryUsed = 0;
    private String name;
    private Thread t;

    public MemoryDaemon(){
        this.name = UUID.randomUUID().toString();
        System.out.println("Creating " +  name );
    }

    public MemoryDaemon( String name) {
        this.name = name;
        System.out.println("Creating " +  name );
    }

    @Override
    public void run() {
        Runtime t = Runtime.getRuntime();
        long used;

        try {
            while (true) {
                used = t.totalMemory() - t.freeMemory();
                if (used != memoryUsed) {
//                    System.out.println("\tMemory used: " + used);
                    memoryUsed += used;
                }
                TimeUnit.SECONDS.sleep(1);
            }
        } catch (Exception e) {
            System.out.println("Thread " + name + " interupted!");
        }
        System.out.println("Thread " + name + " exited!");
    }

    public void start() {
        if (t == null){
            t = new Thread(this, name);
            t.setDaemon( true);
            t.start();
            System.out.println("Thread " + name + " is running!");
        }
    }

    //getters
    public long getMemoryUsed() {
        return memoryUsed;
    }

    public String getName() {
        return name;
    }

    //setters
    public void setName(String name) {
        this.name = name;
        t.setName( name);
    }
}
