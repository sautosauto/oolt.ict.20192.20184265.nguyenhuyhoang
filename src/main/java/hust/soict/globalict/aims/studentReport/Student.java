package hust.soict.globalict.aims.studentReport;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Student {
    private String studentID;
    private String studentName;
    private String birthday;
    private float GPA;


    public String getStudentID() {
        return studentID;
    }

    public String getStudentName() {
        return studentName;
    }

    public String getBirthday() {
        return birthday;
    }

    public float getGPA() {
        return GPA;
    }

    public void setStudentID(String studentID) {
        studentID = studentID.trim();
        this.studentID = studentID;
    }

    public void setStudentName(String studentName) {
        studentName = studentName.trim();
        this.studentName = studentName;
    }

    public void setBirthday(String birthday) throws IllegalBirthdayException {
        birthday = birthday.trim();
        if (!isBirthday(birthday)) {
            throw new IllegalBirthdayException();
        }
        this.birthday = birthday;
    }


    public void setGPA(float GPA) throws IllegalGPAException {
        if (!isGPA(GPA)) {
            throw new IllegalGPAException();
        }
        this.GPA = GPA;
    }

    private boolean isBirthday(String birthday) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setLenient(false);
        try {
            sdf.parse(birthday);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

    private boolean isGPA(float gpa) {
        return gpa >= 0 & gpa <= 4;
    }

}
