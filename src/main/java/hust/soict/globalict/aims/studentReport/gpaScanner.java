package hust.soict.globalict.aims.studentReport;

import java.util.Scanner;

public class gpaScanner {
    public static void main(String[] args) throws IllegalBirthdayException, IllegalGPAException {
        Student my_Student = new Student();

        System.out.println("Please enter student info: ");

        Scanner scanner = new Scanner(System.in);
        System.out.print("StudentID: ");
        my_Student.setStudentID(scanner.nextLine());

        System.out.print("Student name: ");
        my_Student.setStudentName(scanner.nextLine());

        System.out.print("Student birthday: ");
        my_Student.setBirthday(scanner.nextLine());

        System.out.print("Student GPA: ");
        my_Student.setGPA(scanner.nextFloat());

        System.out.println("Student profile: \n"
                + "\t" + my_Student.getStudentID() + "\n" +
                "\t" + my_Student.getStudentName() + "\n" +
                "\t" + my_Student.getBirthday() + "\n" +
                "\t" + my_Student.getGPA()
        );
    }
}
