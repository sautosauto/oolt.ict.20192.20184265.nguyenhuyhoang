package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class Order {

    public static final int MAX_NUMBER_ORDERED = 10;
    public static final int MAX_LIMITED_ORDERS = 5;

    private static int nbOrders = 0;

    private List<Media> itemsOrdered = new ArrayList<>();
    private String dateOrdered;


    public Order() {
        if ( !isValidOrder())
            throw new IllegalArgumentException();
        else {
            nbOrders += 1;
            this.dateOrdered = DateTime.now().toString();
            System.out.println("Order created successfully!");
        }
    }


    public void addMedia(Media disc){

        if ( isFull()){
            System.out.println("Full");
        }
        else{
            itemsOrdered.add(disc);
            System.out.println("Add successfull!");
        }
    }

    public void addMedia(Media[] dvdList){

        if ( isFull()){
            System.out.println("Full");
        }
        else if (!isInsertable(dvdList)){
            System.out.println("Insufficient space!");
        }
        else {
            for (Media disc: dvdList) {
                addMedia(disc);
            }
        }
    }

    public void addMedia(Media dvd1, Media dvd2){
        Media[] dvdArray = new Media[]{dvd1, dvd2};
        addMedia(dvdArray);
    }


    public void removeMedia(Media disc){
        if( isEmpty()) System.out.println("Empty!");
        else {
            itemsOrdered.remove(disc);
            System.out.println("Remove successfully!");
        }
    }

    public void removeMedia(String id){
        if( isEmpty()) System.out.println("Empty");
        else {
            for (Media thing : itemsOrdered) {
                if (thing.getId().equals(id) ) itemsOrdered.remove( thing);
                break;
            }
            System.out.println("Remove successfully!");
        }
    }

    public void interactiveRemoveMedia(){
        Scanner x = new Scanner(System.in);
        System.out.println("Input id: ");

        String removeId = x.nextLine().trim();
        removeMedia( removeId);
    }



    public void print(){
        // Print out order in a give format
        System.out.println( "Date: " + dateOrdered);
        System.out.println("Ordered Items:");

        //1. DVD - [Id] - [Title] - [category] - [Director] - [Length]: [Price] $
        for (int i = 0; i < itemsOrdered.size(); i++){
            System.out.print(i+1 + ". ");
            itemsOrdered.get(i).print();
        }

        System.out.println("Total cost: " + totalCost());
    }


    public float totalCost(){
        float sum = 0;
        for (Media media : itemsOrdered) {
            sum += media.getCost();
        }
        return sum;
    }


    public DigitalVideoDisc getALuckyItem(){
        //Make cost of one item 0

        int random_index = new Random().nextInt(itemsOrdered.size());
        DigitalVideoDisc freeDisc = (DigitalVideoDisc) itemsOrdered.get( random_index);
        freeDisc.setCost(0);
        System.out.println("Luckied!");
        return freeDisc;
    }


    //Validators
    protected static boolean isValidOrder()  { return nbOrders < MAX_LIMITED_ORDERS; }

    private boolean isFull()  { return itemsOrdered.size() == MAX_NUMBER_ORDERED; }

    private boolean isEmpty(){
        return itemsOrdered.isEmpty();
    }

    boolean isInsertable(Media[] dvds){
        return (dvds.length + itemsOrdered.size()) <= MAX_NUMBER_ORDERED;
    }


    //Getters
    public String getDateOrdered() {
        return dateOrdered;
    }


    //Setters
    public void setDateOrdered(String dateOrdered) {
        this.dateOrdered = dateOrdered;
    }

}
