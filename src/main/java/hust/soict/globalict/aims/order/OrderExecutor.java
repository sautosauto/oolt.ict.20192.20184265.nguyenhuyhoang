package hust.soict.globalict.aims.order;

import hust.soict.globalict.aims.PlayerException;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;

import java.util.InputMismatchException;
import java.util.Scanner;

public class OrderExecutor {

    Order myOrder = null;
    int choice = 0;

    public boolean execute() {

        switch (choice){
            case 1: {
                if (myOrder == null) {
                    this.myOrder = new Order();
                }
                else {
                    System.out.println("Order has already been created!");
                }
                break;
            }

            case 2: {
                try {
                    Media m = getMediaChoice();
                    myOrder.addMedia( m.interactiveInitialize());

                } catch (NullPointerException | PlayerException e) {
                    System.out.println("No order created!");

                    e.printStackTrace();
                    System.out.println(e.getMessage());
                    System.out.println(e.toString());

                }
                break;
            }

            case 3: {
                try {
                    myOrder.interactiveRemoveMedia();
                    break;
                } catch (NullPointerException e){
                    e.printStackTrace();
                    System.exit(1);
                }
            }

            case 4: {
                try {
                    myOrder.print();
                    break;
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            default: break;
        }

        makeDelay();
        return ( 0 < choice && choice <5);
    }

    private void makeDelay() {

        try {
            Thread.sleep(500);
        }
        catch (Exception e){
            System.out.println("Interupted!");
            e.printStackTrace();
            System.exit(1);
        }
    }


    private Media getMediaChoice(){
        Scanner x = new Scanner( System.in);
        System.out.println("Input 1 for book, 2 for compact disc, "
                            +" other for DigitalVideoDisc");

        int choice = 0;
        try {
            choice = x.nextInt();
        } catch (InputMismatchException ignored){}

        Media m;
        switch ( choice){
            case 1:{
                m = new Book();
                break;
            }
            case 2:{
                m = new CompactDisc();
                break;
            }
            default:{
                m = new DigitalVideoDisc();
                break;
            }
        }
        return m;
    }

    public void setChoice(){

        Scanner s = new Scanner( System.in);

        int choice = 0;
        try {
            choice = s.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Illegal format");
            e.printStackTrace();
            System.exit(1);
        }

        this.choice = choice;
    }
}
