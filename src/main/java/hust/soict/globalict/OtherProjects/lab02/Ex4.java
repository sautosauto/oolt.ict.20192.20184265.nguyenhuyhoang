package hust.soict.globalict.OtherProjects.lab02;

import org.apache.commons.lang3.StringUtils;

import javax.swing.*;

class StarPrint{
    public static void main(String[] args) {
        var test = new StarPrinter();
        test.getLine_num();
        test.printStar();
    }
}

class StarPrinter {
    private int line_num;

    void getLine_num(){
        line_num = Integer.parseInt(JOptionPane.showInputDialog(null, "Enter n: "));
    }

    void printStar(){
        String msg = "";
        for (int i = 0; i < line_num; i++) {

            int space_num = line_num - i;
            int star_num = 1 + i*2;

            msg +=( StringUtils.repeat(" ", space_num) + "\t" +
                                StringUtils.repeat("*", star_num)) +"\n";
        }

        JOptionPane.showMessageDialog(null, msg);
        System.exit(0);
    }
}
