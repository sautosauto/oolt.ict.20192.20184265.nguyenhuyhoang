package hust.soict.globalict.OtherProjects.lab02;

import javax.swing.*;
import java.util.Arrays;

public class Ex7 {
    public static void main(String[] args) {
        var matrix1 = new Matrix("Matrix1");
        matrix1.getMatrix();

        var matrix2 = new Matrix("Matrix2");
        matrix2.getMatrix();

        var sumMatrix = matrix1.sum(matrix2);
        sumMatrix.displayMatrix();
    }
}

class Matrix {
    String name;
    int row, column;
    double[][] matrix;

    public Matrix(String name) {
        //Basic
        this.name = name.trim().toLowerCase();
    }

    public Matrix(String name, int row, int column, double[][] matrix) {
        this.name = name.trim().toLowerCase();
        this.row = row;
        this.column = column;
        this.matrix = matrix;
    }

    public Matrix(Matrix a) {
        //Clone
        this.name = a.name;
        this.row = a.row;
        this.column = a.column;
        this.matrix = a.matrix;
    }

    void getMatrix() {
        row = Integer.parseInt(JOptionPane.showInputDialog(null,
                "Input number of rows: ", name, JOptionPane.INFORMATION_MESSAGE));

        column = Integer.parseInt(JOptionPane.showInputDialog(null,
                "Input number of columns: ", name, JOptionPane.INFORMATION_MESSAGE));

        validateSize();

        matrix = new double[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {

                double numInput = Double.parseDouble(JOptionPane.showInputDialog(null,
                        "Input value of row " + i + ", column " + j + ": ",
                        name, JOptionPane.INFORMATION_MESSAGE));
                matrix[i][j] = numInput;
            }
        }
    }

    private void validateSize() {
        if (row <= 0 || column <= 0) {
            System.exit(0);
        }
    }

    Matrix sum(Matrix a) {

        checkSummable(a);

        double[][] sumMatrix = new double[a.row][a.column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                sumMatrix[i][j] = this.matrix[i][j] + a.matrix[i][j];
            }
        }
        return new Matrix("sumMatrix", a.row, a.column, sumMatrix);
    }

    private void checkSummable(Matrix a) {
        if( !(this.column == a.column && this.row == a.row) ){
            System.exit(0);
        }

    }

    void displayMatrix() {
        StringBuilder arrayString  = new StringBuilder("[\n");
        for (int i = 0; i < row; i++) {
            arrayString.append(Arrays.toString(matrix[i])).append(",\n");
        }
        arrayString.append(" \t\t]");

        JOptionPane.showMessageDialog(null, arrayString.toString(), "Sum", JOptionPane.INFORMATION_MESSAGE);
        System.exit(0);
    }
}

