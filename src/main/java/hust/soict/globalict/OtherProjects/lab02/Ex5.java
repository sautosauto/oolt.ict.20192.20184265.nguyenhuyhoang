package hust.soict.globalict.OtherProjects.lab02;

import javax.swing.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Ex5 {
    public static void main(String[] args) {
        var mycalender = new Calender();
        mycalender.getMonth();
        mycalender.showDays();
    }
}

class Calender{

    private int month;
    private int year;

    void getMonth(){
        month = Integer.parseInt(JOptionPane.showInputDialog( null,
                                                "Input month: "));
        year = Integer.parseInt(JOptionPane.showInputDialog( null,
                "Input year: "));
        validateInput();
    }

    void validateInput(){
        if( month<=0 || month>12 || year<=0){
            JOptionPane.showMessageDialog(null, "Invalid input");
            getMonth();
        }
    }

    void showDays(){
        final Set<Integer> list_31 = new HashSet<Integer>(Arrays.asList( 1, 3, 5, 7, 8, 10, 12));
        final Set<Integer> list_30 = new HashSet<Integer>(Arrays.asList( 4, 6, 9, 11));

        if( list_31.contains( month)){
            JOptionPane.showMessageDialog(null, "Month has 31 days");
        }
        else if (list_30.contains( month)){
            JOptionPane.showMessageDialog(null, "Month has 30 days");
        }
        else if( year %4 == 0 && year %100 != 0){
            JOptionPane.showMessageDialog(null, "Month has 29 days");
        }
        else {
            JOptionPane.showMessageDialog(null, "Month has 28 days");
        }
    System.exit(0);
    }
}