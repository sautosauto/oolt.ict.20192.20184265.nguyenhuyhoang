package hust.soict.globalict.OtherProjects.lab02;

import javax.swing.*;
import java.util.Arrays;

public class Ex6 {
    public static void main(String[] args) {
        var myArraySort = new ArraySort();

        myArraySort.getArray();
        myArraySort.displayArray();
    }
}

class ArraySort{
    private int arraySize;
    private int [] myArray;

    void getArray(){
        arraySize = Integer.parseInt(JOptionPane.showInputDialog( null,
                                    "Input array size: "));
        validateArraySize();

        myArray = new int[arraySize];

        for (int i = 0; i < arraySize; i++) {
            myArray[i] = Integer.parseInt(
                    JOptionPane.showInputDialog( null, "Input number " + i +" :"));
        }
        sortArray();
    }

    private void validateArraySize(){
        if (arraySize <0 ){
            System.exit(0);
        }
    }

    private void sortArray(){
        Arrays.sort(myArray);
    }

    void displayArray(){
        JOptionPane.showMessageDialog( null,
                            "Sorted array: " + Arrays.toString( myArray));

        System.exit(0);
    }
}
