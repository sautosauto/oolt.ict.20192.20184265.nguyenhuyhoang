package hust.soict.globalict.OtherProjects.lab01;

import javax.swing.*;

public class Ex4 {
    public static void main(String[] args) {
        String strNum1, strNum2;
        String notification = "You've just entered: ";

        strNum1 = JOptionPane.showInputDialog(null,
                "Please enter the first number: ", "Input the first number",
                JOptionPane.INFORMATION_MESSAGE);
        notification += strNum1 + " and ";

        strNum2 = JOptionPane.showInputDialog(null,
                "Please enter the second number: ", "Input the second number",
                JOptionPane.INFORMATION_MESSAGE);
        notification += strNum2;

        JOptionPane.showMessageDialog(null, notification,
                "Show two numbers", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}