package hust.soict.globalict.OtherProjects.lab01;

import javax.swing.*;

class EquationSolver{
    public static void main(String[] args) {
        var menu = new Menu();
        menu.exec();

        System.exit(0);
    }

}

class Menu {
     void exec() {
        String option;
        do {
            option = displayMenu();

            switch (option) {
                case "1": {
                    var solver = new firstOrderOneVar(inputMenu11());
                    solver.displayAnswer();
                    break;
                }
                case "2": {
                    var solver = new firstOrderTwoVar(inputMenu12());
                    solver.displayAnswer();
                    break;
                }
                case "3": {
                    var solver = new secondOrderOneVar(inputMenu21());
                    solver.displayAnswer();
                    break;
                }
                default:
                    break;
            }
        }
        while (0 < Double.parseDouble(option)  && Double.parseDouble(option)< 4);
    }

    private String displayMenu(){
        String option = JOptionPane.showInputDialog(null,
                "Choose option:\n"
                        +"1. Solve for x:\n" +
                        "ax + b = c\n"

                        +"2. Solve for x, y:\n" +
                        "ax + by = c\n" +
                        "dx + ey = f\n" +

                        "3. Solve for x:\n" +
                        "a* x^2 + bx = c\n" +
                        "Else: Exit!",
                "ex1.Menu", JOptionPane.INFORMATION_MESSAGE);
        return  option;
    }

    private double[] inputMenu11(){
        double a = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter a: ", "ax + b = c",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double b = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter b: ", "ax + b = c",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double c = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter c: ", "ax + b = c",
                JOptionPane.INFORMATION_MESSAGE).trim());

        return new double[] {a, b, c};
    }

    private double[] inputMenu12(){

        double a = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter a: ", "ax + by = c",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double b = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter b: ", "ax + by = c",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double c = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter c: ", "ax + by = c",
                JOptionPane.INFORMATION_MESSAGE).trim());


        double d = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter d: ", "dx + ey = f",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double e = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter e: ", "dx + ey = f",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double f = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter f: ", "dx + ey = f",
                JOptionPane.INFORMATION_MESSAGE).trim());

        return new double[] {a, b, c, d, e, f};
    }

    private double[] inputMenu21(){
        double a = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter a: ", "a* x^2 + bx + c = 0",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double b = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter b: ",  "a* x^2 + bx + c = 0",
                JOptionPane.INFORMATION_MESSAGE).trim());

        double c = Double.parseDouble(JOptionPane.showInputDialog(null,
                "Please enter c: ",  "a* x^2 + bx + c = 0",
                JOptionPane.INFORMATION_MESSAGE).trim());

        return new double[] {a, b, c};
    }

}

class firstOrderOneVar{
    private double a, b, c;
    // ax + b = c
    public firstOrderOneVar( double[] arr){
        this.a = arr[0];
        this.b = arr[1];
        this.c = arr[2];
    }

    private double Answer() {
        if (this.a != 0){
            return (this.c - this.b) / this.a;
        }
        else {
            return Double.NaN;
        }
    }

    void displayAnswer(){
        if (a == 0){
            JOptionPane.showMessageDialog(null, "No soluiton");
        }
        else{
            JOptionPane.showMessageDialog(null, "Answer: " + this.Answer());
        }
        System.exit(0);
    }
}


class firstOrderTwoVar{
    private double a, b, c, d, e, f;
    // ax + by = c
    // dx + ey = f
    public firstOrderTwoVar( double[] arr){
        this.a = arr[0];
        this.b = arr[1];
        this.c = arr[2];
        this.d = arr[3];
        this.e = arr[4];
        this.f = arr[5];
    }

    private double[] Answer() {
        // Call when there's absolutely have a solution
        double x = Double.NaN;
        double y = Double.NaN;

        if( d==0 ){
            if( e!= 0 ){
                y = f/e;
                if( a!=0 ){
                    x = (c - b*y)/a;
                }
            }
        }
        else {
            y = (c - a / d) / (b - e * a / d);
            if (a != 0) {
                x = (c - b * y) / a;
            }
        }

        return new double[]{x, y};
    }

     void displayAnswer(){
        if ( b*d == a*e) {
            if (b * f == c * e) {
                JOptionPane.showMessageDialog(null,
                        "Infinitely many solutions satisfying " + a + "x + " + b + " y = " + c);

            } else {
                JOptionPane.showMessageDialog(null,
                        "No solution!");
            }
        }
        else{
            double x = this.Answer()[0];
            double y = this.Answer()[1];
            JOptionPane.showMessageDialog(null, "x = " + x + "\ny = " + y);
        }
        System.exit(0);
    }
}

class secondOrderOneVar {
    private double a;
    private double b;
    private double c;
    private double delta;

    // a*x^2 + bx + c = 0
    public secondOrderOneVar(double[] arr) {
        this.a = arr[0];
        this.b = arr[1];
        this.c = arr[2];
        this.delta = getDelta();
    }

    private double getDelta() {
        return b * b - 4 * a * c;
    }


    private int getFlag() {
        // flag = 0: No solution
        // flag = 1: 1 solution
        // flag = 2: 2 solutions

        if (delta < 0) {
            return 0;
        } else if (delta == 0) {
            return 1;
        } else {
            return 2;
        }
    }

    private double[] Answer(int flag) {
        double[] arr = new double[2];

        if (flag == 1) {
            arr[0] = b / (2 * a);
        } else {
            arr[0] = (-b + Math.sqrt(delta)) / (2 * a);
            arr[1] = (-b - Math.sqrt(delta)) / (2 * a);
        }

        return arr;
    }

     void displayAnswer() {

        int flag = getFlag();

        if (flag == 0) {
            JOptionPane.showMessageDialog(null, "No solution!");
        } else if (flag == 1) {
            JOptionPane.showMessageDialog(null, "x = " + Answer(flag)[0]);
        } else {
            JOptionPane.showMessageDialog(null, "x = " + Answer(flag)[0] + " or " + Answer(flag)[1]);
        }

        System.exit(0);
    }
}