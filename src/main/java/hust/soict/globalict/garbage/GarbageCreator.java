package hust.soict.globalict.garbage;

import java.util.concurrent.TimeUnit;

public class GarbageCreator {
    private static int garbageCount = 0;
    private final long[] someNumber = new long[100];

    public GarbageCreator() throws OutOfMemoryError {
        if (garbageCount > 6) {
            throw new OutOfMemoryError("Oops! Too much garbage!");
        }
        garbageCount += 1;
    }

    public static void showGarbageCount() {
        System.out.println("No of garbage: " + garbageCount);
    }


    @Deprecated
    protected void finalize() throws Throwable {
        garbageCount--;
        System.out.println(garbageCount + " garbage left");
    }


    public static void main(String[] args) {

        try {
            for (int i = 0; i < 5; i++) {
                var ptr = new GarbageCreator();
                showGarbageCount();

                try {
                    TimeUnit.MILLISECONDS.sleep(300);
                } catch (Exception e){
                    System.out.println("Something Timeoutly went wrong");
                }
            }


            //Raise error without calling collectGarbage()
            NoGarbage.collectGarbage();


            showGarbageCount();

            for (int i = 0; i < 4; i++) {
                var ptr = new GarbageCreator();
                showGarbageCount();

                try {
                    TimeUnit.MILLISECONDS.sleep(300);
                } catch (Exception e){
                    System.out.println("Something Timeoutly went wrong");
                }

            }
        } catch (OutOfMemoryError e) {
            System.out.println("Something's OutofMemorily wrong!");
        }

        System.exit(0);
    }

}
