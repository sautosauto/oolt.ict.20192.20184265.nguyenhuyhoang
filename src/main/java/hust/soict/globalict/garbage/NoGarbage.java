package hust.soict.globalict.garbage;

import java.util.concurrent.TimeUnit;

public class NoGarbage {
    public static void collectGarbage() {
        System.out.println("Maybe collecting garbage!");
        System.gc();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (Exception e) {
            System.out.println("Something went wrong!");
        }
    }
}
