package hust.soict.globalict.test.book;

import hust.soict.globalict.aims.media.Book;

import java.util.Arrays;
import java.util.List;

public class BookTest {
    public static void main(String[] args) {
        List<String> authors = Arrays.asList("Michel Bond", "Jams Matchetee");
        Book myBook = new Book("A holy book", "Horror", 600F, authors);

        myBook.setContent("This is a secret, really secret! This secretness of a secret " +
                "shall remain! No(although yes!) is my secret not a secret...." +
                "Or isn't it a secret?!?");

        System.out.println(myBook.toString());
    }
}
