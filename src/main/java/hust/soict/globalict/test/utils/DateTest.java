package hust.soict.globalict.test.utils;

import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;
import org.joda.time.DateTime;

public class DateTest {
    public static void main(String[] args) {

        /* Test constructor */
        var myDate1 = new MyDate();
        var myDate2 = new MyDate(6, 5, 2020);
        var myDate3 = new MyDate();

        /* Test 'accept' method */
//        myDate3.accept();

        /* Test print method */
        myDate1.printDigitFormat();
        myDate2.printDigitFormat();
        myDate3.printDigitFormat();

        /* Test setter */
        myDate1.setDay(29);
        myDate1.setMonth(4);
        myDate1.setYear(2016);

        /* Test getter */
        myDate1.printDigitFormat();


        myDate2.setDay("first");
        myDate2.setMonth("October");
        myDate2.setYear("twenty o one");

//        myDate2.print();

        /* Test compareDate() */
        System.out.println(DateUtils.compareDate(myDate1.getDateTime(), myDate2.getDateTime()));

        /* Test sortDate */
        DateTime[] arrDate = new DateTime[3];
        arrDate[0] = myDate1.getDateTime();
        arrDate[1] = myDate2.getDateTime();
        arrDate[2] = myDate3.getDateTime();

        DateUtils.sortDate(arrDate);

        for (DateTime x : arrDate) {
            System.out.println(x.toString());
        }

        System.exit(0);

    }
}
